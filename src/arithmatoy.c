#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

const char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t lhs_len = strlen(lhs);
    size_t rhs_len = strlen(rhs);
    size_t max_len = lhs_len > rhs_len ? lhs_len : rhs_len;
    char *result = (char *)malloc(max_len + 2);
    int carry = 0;
    int pos = 0;

    if (VERBOSE) {
        fprintf(stderr, "********************\nADD: BASE=%d, LHS=%s, RHS=%s\n********************\n", base, lhs, rhs);
    }

    for (int i = 0; i < max_len || carry; i++) {
        int lhs_digit = i < lhs_len ? get_digit_value(lhs[lhs_len - 1 - i]) : 0;
        int rhs_digit = i < rhs_len ? get_digit_value(rhs[rhs_len - 1 - i]) : 0;

        int sum = lhs_digit + rhs_digit + carry;
        carry = sum / base;
        sum = sum % base;

        result[pos++] = to_digit(sum);

        if (VERBOSE) {
            fprintf(stderr, " => ADD: LHS=%d, RHS=%d, SUM=%d, CARRY=%d\n", lhs_digit, rhs_digit, sum, carry);
        }
    }

    result[pos] = '\0';
    reverse(result);

    if (VERBOSE) {
        fprintf(stderr, "********************\nADD: RESULT=%s\n********************\n", result);
    }

    return (char *)drop_leading_zeros(result);
}


const char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t lhs_len = strlen(lhs);
    size_t rhs_len = strlen(rhs);
    char *result = (char *)malloc(lhs_len + 1);
    int borrow = 0;
    int pos = 0;

    if (VERBOSE) {
        fprintf(stderr, "********************\nSUB: BASE=%d, LHS=%s, RHS=%s\n********************\n", base, lhs, rhs);
    }

    for (int i = 0; i < lhs_len; i++) {
        int lhs_digit = get_digit_value(lhs[lhs_len - 1 - i]);
        int rhs_digit = i < rhs_len ? get_digit_value(rhs[rhs_len - 1 - i]) : 0;
        int diff = lhs_digit - rhs_digit - borrow;
        
        if (diff < 0) {
            diff += base;
            borrow = 1;
        } else {
            borrow = 0;
        }

        result[pos++] = to_digit(diff);

        if (VERBOSE) {
            fprintf(stderr, " => SUB: LHS=%d, RHS=%d, DIFF=%d, BORROW=%d\n", lhs_digit, rhs_digit, diff, borrow);
        }
    }

    if (borrow != 0) {
        free(result);
        return NULL;
    }

    result[pos] = '\0';
    reverse(result);

    if (VERBOSE) {
        fprintf(stderr, "********************\nSUB: result=%s\n********************\n", result);
    }

    return (char *)drop_leading_zeros(result);
}



const char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t lhs_len = strlen(lhs);
    size_t rhs_len = strlen(rhs);
    size_t result_len = lhs_len + rhs_len;
    char *result = (char *)calloc(result_len + 1, sizeof(char));
    memset(result, '0', result_len);

    if (VERBOSE) {
        fprintf(stderr, "********************\nMUL: BASE=%d, LHS=%s, RHS=%s\n********************\n", base, lhs, rhs);
    }

    for (size_t i = 0; i < rhs_len; i++) {
        int rhs_digit = get_digit_value(rhs[rhs_len - 1 - i]);
        int carry = 0;
        for (size_t j = 0; j < lhs_len || carry; j++) {
            int lhs_digit = j < lhs_len ? get_digit_value(lhs[lhs_len - 1 - j]) : 0;
            int existing = get_digit_value(result[result_len - 1 - (i + j)]);

            int product = rhs_digit * lhs_digit + existing + carry;
            carry = product / base;
            result[result_len - 1 - (i + j)] = to_digit(product % base);

            if (VERBOSE) {
                fprintf(stderr, " => MUL: LHS=%d, RHS=%d, PROD=%d, CARRY=%d\n", lhs_digit, rhs_digit, product, carry);
            }
        }
    }

    if (VERBOSE) {
        fprintf(stderr, "********************\nMUL: result=%s\n********************\n", result);
    }
    return drop_leading_zeros(result);
}


// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
