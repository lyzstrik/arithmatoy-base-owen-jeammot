add 35 a4pahak1i3mppasq089x3k3j9mgubc2f4ro3n2ihecnn8l0i3r m8g9tfvy18t78xxw5a86velfgmtwv1p86xs5o7pwja1nf9ufsc --verbose
a4pahak1i3mppasq089x3k3j9mgubc2f4ro3n2ihecnn8l0i3r + m8g9tfvy18t78xxw5a86velfgmtwv1p86xs5o7pwja1nf9ufsc = wd6kbqh0jcgwy9rn5ii4yyoyqabs7drnbqh9ca9exmpbnuuxw4
ADD: base=35, lhs=a4pahak1i3mppasq089x3k3j9mgubc2f4ro3n2ihecnn8l0i3r, rhs=m8g9tfvy18t78xxw5a86velfgmtwv1p86xs5o7pwja1nf9ufsc
ADD: lhs_digit=27, rhs_digit=12, sum=4, carry=1
ADD: lhs_digit=3, rhs_digit=28, sum=32, carry=0
ADD: lhs_digit=18, rhs_digit=15, sum=33, carry=0
ADD: lhs_digit=0, rhs_digit=30, sum=30, carry=0
ADD: lhs_digit=21, rhs_digit=9, sum=30, carry=0
ADD: lhs_digit=8, rhs_digit=15, sum=23, carry=0
ADD: lhs_digit=23, rhs_digit=23, sum=11, carry=1
ADD: lhs_digit=23, rhs_digit=1, sum=25, carry=0
ADD: lhs_digit=12, rhs_digit=10, sum=22, carry=0
ADD: lhs_digit=14, rhs_digit=19, sum=33, carry=0
ADD: lhs_digit=17, rhs_digit=32, sum=14, carry=1
ADD: lhs_digit=18, rhs_digit=25, sum=9, carry=1
ADD: lhs_digit=2, rhs_digit=7, sum=10, carry=0
ADD: lhs_digit=23, rhs_digit=24, sum=12, carry=1
ADD: lhs_digit=3, rhs_digit=5, sum=9, carry=0
ADD: lhs_digit=24, rhs_digit=28, sum=17, carry=1
ADD: lhs_digit=27, rhs_digit=33, sum=26, carry=1
ADD: lhs_digit=4, rhs_digit=6, sum=11, carry=0
ADD: lhs_digit=15, rhs_digit=8, sum=23, carry=0
ADD: lhs_digit=2, rhs_digit=25, sum=27, carry=0
ADD: lhs_digit=12, rhs_digit=1, sum=13, carry=0
ADD: lhs_digit=11, rhs_digit=31, sum=7, carry=1
ADD: lhs_digit=30, rhs_digit=32, sum=28, carry=1
ADD: lhs_digit=16, rhs_digit=29, sum=11, carry=1
ADD: lhs_digit=22, rhs_digit=22, sum=10, carry=1
ADD: lhs_digit=9, rhs_digit=16, sum=26, carry=0
ADD: lhs_digit=19, rhs_digit=15, sum=34, carry=0
ADD: lhs_digit=3, rhs_digit=21, sum=24, carry=0
ADD: lhs_digit=20, rhs_digit=14, sum=34, carry=0
ADD: lhs_digit=3, rhs_digit=31, sum=34, carry=0
ADD: lhs_digit=33, rhs_digit=6, sum=4, carry=1
ADD: lhs_digit=9, rhs_digit=8, sum=18, carry=0
ADD: lhs_digit=8, rhs_digit=10, sum=18, carry=0
ADD: lhs_digit=0, rhs_digit=5, sum=5, carry=0
ADD: lhs_digit=26, rhs_digit=32, sum=23, carry=1
ADD: lhs_digit=28, rhs_digit=33, sum=27, carry=1
ADD: lhs_digit=10, rhs_digit=33, sum=9, carry=1
ADD: lhs_digit=25, rhs_digit=8, sum=34, carry=0
ADD: lhs_digit=25, rhs_digit=7, sum=32, carry=0
ADD: lhs_digit=22, rhs_digit=29, sum=16, carry=1
ADD: lhs_digit=3, rhs_digit=8, sum=12, carry=0
ADD: lhs_digit=18, rhs_digit=1, sum=19, carry=0
ADD: lhs_digit=1, rhs_digit=34, sum=0, carry=1
ADD: lhs_digit=20, rhs_digit=31, sum=17, carry=1
ADD: lhs_digit=10, rhs_digit=15, sum=26, carry=0
ADD: lhs_digit=17, rhs_digit=29, sum=11, carry=1
ADD: lhs_digit=10, rhs_digit=9, sum=20, carry=0
ADD: lhs_digit=25, rhs_digit=16, sum=6, carry=1
ADD: lhs_digit=4, rhs_digit=8, sum=13, carry=0
ADD: lhs_digit=10, rhs_digit=22, sum=32, carry=0
ADD: result=wd6kbqh0jcgwy9rn5ii4yyoyqabs7drnbqh9ca9exmpbnuuxw4
