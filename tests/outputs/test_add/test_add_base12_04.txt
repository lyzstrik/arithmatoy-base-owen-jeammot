add 12 17456327643a00b6246516531853113681a036a38891118361ba12737ab694224a855925 11a96b0b9840471278a925344ab7ab35814126524966804b3392043b3817525abb63814 --verbose
17456327643a00b6246516531853113681a036a38891118361ba12737ab694224a855925 + 11a96b0b9840471278a925344ab7ab35814126524966804b3392043b3817525abb63814 = 18643a18620205675033a8a66142902a19b44948b1677988553732b7727849483a7b9539
ADD: base=12, lhs=17456327643a00b6246516531853113681a036a38891118361ba12737ab694224a855925, rhs=11a96b0b9840471278a925344ab7ab35814126524966804b3392043b3817525abb63814
ADD: lhs_digit=5, rhs_digit=4, sum=9, carry=0
ADD: lhs_digit=2, rhs_digit=1, sum=3, carry=0
ADD: lhs_digit=9, rhs_digit=8, sum=5, carry=1
ADD: lhs_digit=5, rhs_digit=3, sum=9, carry=0
ADD: lhs_digit=5, rhs_digit=6, sum=11, carry=0
ADD: lhs_digit=8, rhs_digit=11, sum=7, carry=1
ADD: lhs_digit=10, rhs_digit=11, sum=10, carry=1
ADD: lhs_digit=4, rhs_digit=10, sum=3, carry=1
ADD: lhs_digit=2, rhs_digit=5, sum=8, carry=0
ADD: lhs_digit=2, rhs_digit=2, sum=4, carry=0
ADD: lhs_digit=4, rhs_digit=5, sum=9, carry=0
ADD: lhs_digit=9, rhs_digit=7, sum=4, carry=1
ADD: lhs_digit=6, rhs_digit=1, sum=8, carry=0
ADD: lhs_digit=11, rhs_digit=8, sum=7, carry=1
ADD: lhs_digit=10, rhs_digit=3, sum=2, carry=1
ADD: lhs_digit=7, rhs_digit=11, sum=7, carry=1
ADD: lhs_digit=3, rhs_digit=3, sum=7, carry=0
ADD: lhs_digit=7, rhs_digit=4, sum=11, carry=0
ADD: lhs_digit=2, rhs_digit=0, sum=2, carry=0
ADD: lhs_digit=1, rhs_digit=2, sum=3, carry=0
ADD: lhs_digit=10, rhs_digit=9, sum=7, carry=1
ADD: lhs_digit=11, rhs_digit=3, sum=3, carry=1
ADD: lhs_digit=1, rhs_digit=3, sum=5, carry=0
ADD: lhs_digit=6, rhs_digit=11, sum=5, carry=1
ADD: lhs_digit=3, rhs_digit=4, sum=8, carry=0
ADD: lhs_digit=8, rhs_digit=0, sum=8, carry=0
ADD: lhs_digit=1, rhs_digit=8, sum=9, carry=0
ADD: lhs_digit=1, rhs_digit=6, sum=7, carry=0
ADD: lhs_digit=1, rhs_digit=6, sum=7, carry=0
ADD: lhs_digit=9, rhs_digit=9, sum=6, carry=1
ADD: lhs_digit=8, rhs_digit=4, sum=1, carry=1
ADD: lhs_digit=8, rhs_digit=2, sum=11, carry=0
ADD: lhs_digit=3, rhs_digit=5, sum=8, carry=0
ADD: lhs_digit=10, rhs_digit=6, sum=4, carry=1
ADD: lhs_digit=6, rhs_digit=2, sum=9, carry=0
ADD: lhs_digit=3, rhs_digit=1, sum=4, carry=0
ADD: lhs_digit=0, rhs_digit=4, sum=4, carry=0
ADD: lhs_digit=10, rhs_digit=1, sum=11, carry=0
ADD: lhs_digit=1, rhs_digit=8, sum=9, carry=0
ADD: lhs_digit=8, rhs_digit=5, sum=1, carry=1
ADD: lhs_digit=6, rhs_digit=3, sum=10, carry=0
ADD: lhs_digit=3, rhs_digit=11, sum=2, carry=1
ADD: lhs_digit=1, rhs_digit=10, sum=0, carry=1
ADD: lhs_digit=1, rhs_digit=7, sum=9, carry=0
ADD: lhs_digit=3, rhs_digit=11, sum=2, carry=1
ADD: lhs_digit=5, rhs_digit=10, sum=4, carry=1
ADD: lhs_digit=8, rhs_digit=4, sum=1, carry=1
ADD: lhs_digit=1, rhs_digit=4, sum=6, carry=0
ADD: lhs_digit=3, rhs_digit=3, sum=6, carry=0
ADD: lhs_digit=5, rhs_digit=5, sum=10, carry=0
ADD: lhs_digit=6, rhs_digit=2, sum=8, carry=0
ADD: lhs_digit=1, rhs_digit=9, sum=10, carry=0
ADD: lhs_digit=5, rhs_digit=10, sum=3, carry=1
ADD: lhs_digit=6, rhs_digit=8, sum=3, carry=1
ADD: lhs_digit=4, rhs_digit=7, sum=0, carry=1
ADD: lhs_digit=2, rhs_digit=2, sum=5, carry=0
ADD: lhs_digit=6, rhs_digit=1, sum=7, carry=0
ADD: lhs_digit=11, rhs_digit=7, sum=6, carry=1
ADD: lhs_digit=0, rhs_digit=4, sum=5, carry=0
ADD: lhs_digit=0, rhs_digit=0, sum=0, carry=0
ADD: lhs_digit=10, rhs_digit=4, sum=2, carry=1
ADD: lhs_digit=3, rhs_digit=8, sum=0, carry=1
ADD: lhs_digit=4, rhs_digit=9, sum=2, carry=1
ADD: lhs_digit=6, rhs_digit=11, sum=6, carry=1
ADD: lhs_digit=7, rhs_digit=0, sum=8, carry=0
ADD: lhs_digit=2, rhs_digit=11, sum=1, carry=1
ADD: lhs_digit=3, rhs_digit=6, sum=10, carry=0
ADD: lhs_digit=6, rhs_digit=9, sum=3, carry=1
ADD: lhs_digit=5, rhs_digit=10, sum=4, carry=1
ADD: lhs_digit=4, rhs_digit=1, sum=6, carry=0
ADD: lhs_digit=7, rhs_digit=1, sum=8, carry=0
ADD: lhs_digit=1, rhs_digit=0, sum=1, carry=0
ADD: result=18643a18620205675033a8a66142902a19b44948b1677988553732b7727849483a7b9539
