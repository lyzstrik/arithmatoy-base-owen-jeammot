def nombre_entier(n: int) -> str:
    return f"{'S' * n}0"


def S(n: str) -> str:
    return f"S{n}"


def addition(a: str, b: str) -> str:    
    return f"{a[:-1] + b}"


def multiplication(a: str, b: str) -> str:
    return "0" if a == "0" or b == "0" else "{}0".format((len(a[:-1]) * len(b[:-1])) * "S")

def facto_ite(n: int) -> int:
    return 1 if n == 0 else n * (lambda f, i: f(f, i))(lambda f, i: 1 if i == 0 else i * f(f, i - 1), n - 1)


def facto_rec(n: int) -> int:
    return 1 if n == 0 else n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    return n if n <= 1 else fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    return 0 if n == 0 else (lambda a=0, b=1: [a := (b, b := a + b)[0] for _ in range(n)])()[n-1]


def golden_phi(n: int) -> int:
    return (lambda x=1: [x := 1 + 1/x for _ in range(n)][-1])()


def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) - 1

def pow(a: float, n: int) -> float:
    return a ** n
